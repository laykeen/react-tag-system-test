import React, { Component, Fragment } from 'react';
import Anime from 'react-anime';

export default class App extends Component {
    constructor(props) {
        super(props);
        const separator = ":";
        this.state = {
            edit_mode: false,
            separator: " "+separator,
            namespaces: {
                "type": {
                    editable: false,
                    i18n: true,
                    short_tag: "t",
                    css: "ns-green",
                    tags: [ "boolean", "string", "number" ]
                },
                "icon": {
                    css: "ns-blue",
                    i18n: true,
                    tags: [ "home", "switch-on", "switch-off" ]
                },
                "function": {
                    editable: false,
                    i18n: true,
                    short_tag: "fn",
                    css: "ns-orange",
                    tags: [ "add", "loop", "subtract" ]
                },
                "component": {
                    editable: false,
                    i18n: true,
                    css: "ns-black",
                    tags: [ "switch", "pulse" ]
                },
                "usermade tag": {
                    css: "ns-silver",
                    tags: [ "testing" ]
                },
                "uncategorized": {
                    hide_namespace: true,
                    css: "ns-gray",
                    tags: [ "custom", "usermade tag" ]
                }
            },
        }
    }
    addTag = (ns, tag) => {
        this.setState((state) => {
            if (state.namespaces[ns] === undefined) {
                let found = false;
                for (var nsp in state.namespaces) {
                    if (state.namespaces.hasOwnProperty(nsp) && state.namespaces[nsp].short_tag === ns) {
                        ns = nsp;
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    state.namespaces[ns] = {
                        css: "ns-silver",
                        tags: []
                    };
                }
            }
            if (state.namespaces[ns].tags.indexOf(tag) === -1) {
                state.namespaces[ns].tags.push(tag);
            }
            return state;
        });
    }
    removeTag = (ns, tag) => {
        this.setState((state) => {
            if (state.namespaces[ns] === undefined) {
                return state;
            }
            var index = state.namespaces[ns].tags.indexOf(tag);
            if (index > -1) {
                state.namespaces[ns].tags.splice(index, 1);
            }
            return state;
        }, () => console.log(this.state));
    }
    editTag = (old_ns, old_tag, ns, tag) => {
        this.setState((state) => {
            if (state.namespaces[old_ns] === undefined) {
                return state;
            }
            var old_index = state.namespaces[ns].tags.indexOf(old_tag);
            if (old_index > -1) {
                this.addTag(ns, tag);
                this.removeTag(old_ns, old_tag);
            }
            return state;
        });
    }
    render() {
        let tag_list = [];
        for (let namespace in this.state.namespaces) {
            if (!this.state.namespaces.hasOwnProperty(namespace)) {
                continue;
            }
            let ns = this.state.namespaces[namespace];
            if (ns.tags !== undefined) {
                for (let i = 0; i < ns.tags.length; i++) {
                    let elem = ns.tags[i];
                    tag_list.push(
                        <LiTag
                            key={`${namespace}_${i}`}
                            ns={namespace}
                            short_tag={ns.short_tag}
                            ns_css={ns.css}
                            hide_namespace={ns.hide_namespace}
                            tag={(elem.tag === undefined) ? elem : elem.tag} tag_css={(elem.css === undefined) ? "" : elem.css}
                            separator={this.state.separator}
                            removeTag={this.removeTag}
                            editable={ns.editable}
                        />
                    );
                }
            }
        }
        return(
            <div className="pa4">
                <ul className="pa0 ma0 flex flex-wrap">
                    {tag_list}
                    <NewTag addTag={this.addTag} separator={this.state.separator.trim()} />
                </ul>
            </div>
        );
    }
}

class LiTag extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editing: false,
            ns: this.props.hide_namespace === true ? "" : this.props.ns,
            tag: this.props.tag
        };
    }
    onDoubleClick() {
        if (this.props.editable !== false) {
            this.setState({ editing: true });
        }
    }
    validateInput(e, prop) {
        if (e.target.value.length > 0) {
            let val = e.target.value.trim();
            if (e.code === "Enter") {
                this.setState({
                    editing: false
                }, () => this.props.editTag(
                    this.props.ns, this.props.tag, this.state.ns, this.state.tag
                ));
            } else {
                this.setState((state) => {
                    state[prop] = val;
                    return state;
                });
            }
        }
    }
    render() {
        return(
            <Anime autoplay={!this.state.editing} easing="easeOutElastic" duration={1000} scale={[ 0.33, 1 ]} rotate={[ 7, 0 ]}>
                <li className={`shadow-hover-light flex ma1 ba bw1 br2 h2 ${this.props.ns_css}`}>
                    {this.state.editing === false ?
                        <p className="f6 ma0 pa0 self-center" onDoubleClick={(e) => this.onDoubleClick(e)}>
                            {this.props.hide_namespace !== true &&
                                <Fragment>
                                    <Fragment>
                                        {this.props.short_tag !== undefined ?
                                            <span className="pl2 tooltip">
                                                {this.props.short_tag}
                                                <span className={`f7 user-select-none tooltip-text ${this.props.ns_css}`}>
                                                    {this.props.ns}
                                                </span>
                                            </span>
                                            :
                                            <span className="pl2">
                                                {this.props.ns}
                                            </span>
                                        }
                                    </Fragment>
                                    <span className="b f7 pr1 user-select-none">
                                        {this.props.separator}
                                    </span>
                                </Fragment>
                            }
                            <span className={`b ${this.props.tag_css} ${this.props.hide_namespace === true ? "pl2" : ""} ${this.props.editable === false ? "pr2" : ""}`}>
                                {this.props.tag}
                            </span>
                        </p>
                        :
                        <p className="f6 ma0 pa0 self-center">
                            <input type="text" className="w4 mh1 tc bg-transparent bn pl2 white" value={this.state.ns} onChange={(e) => this.validateInput(e, "ns")} />
                            <span className="b f7 pr1 user-select-none">
                                {this.props.separator}
                            </span>
                            <input className={`w4 mh1 tc w-auto bg-transparent bn b ${this.props.tag_css} ${this.props.hide_namespace === true ? "pl2" : ""} white`} value={this.state.tag} onChange={(e) => this.validateInput(e, "tag")} />
                        </p>
                    }
                    {this.props.editable !== false &&
                        <button className="pointer self-center b bn ma1 w1 bg-transparent dark-red f7 glow-dark-red" onClick={() => this.props.removeTag(this.props.ns, this.props.tag)}>
                            x
                        </button>
                    }
                </li>
            </Anime>
        );
    }
}

class NewTag extends Component {
    constructor(props) {
        super(props);
        this.state = {
            valid: true,
        };
    }
    getSavedInput(pos) {
        const ss_key = "tag_input_history";
        let old_input = sessionStorage.getItem(ss_key);
        if (old_input === null) {
            return "";
        } else {
            old_input = JSON.parse(old_input);
        }
        sessionStorage.setItem(ss_key, old_input);
        return old_input;
        // TODO
    }
    pushSavedInput(input) {
        const ss_key = "tag_input_history";
        let old_input = sessionStorage.getItem(ss_key);
        if (old_input === null) {
            old_input = { index: 0, tags: [ "" ] };
        } else {
            old_input = JSON.parse(old_input);
        }
        old_input.tags.push(input);
        sessionStorage.setItem(ss_key, old_input);
        // TODO
    }
    validateInput(e) {
        /*if (e.key === "ArrowDown") {
            this.getSavedInput(1);
        } else if (e.key === "ArrowUp") {
            this.getSavedInput(-1);
        } else*/ if (e.target.value.length > 0) {
            if (e.code === "Enter") {
                let val = e.target.value.trim();
                let values = val.split(this.props.separator);
                if (val.length === 0 || values.length > 2) {
                    return this.setState({ valid: false });
                }
                if (values.length === 1) {
                    values[1] = values[0];
                    values[0] = "uncategorized";
                }
                this.props.addTag(values[0], values[1]);
                e.target.value = "";
            }
        }
        this.setState({ valid: true });
    }
    render() {
        return(
            <Anime easing="easeOutElastic" duration={1000} scale={[ 0.33, 1 ]} rotate={[ 7, 0 ]}>
                <li className="flex ma1 bw1 br2 h2">
                    <input type="text"
                        className={`pl2 ba br2 f6 underline ${this.state.valid === true ? "bg-light-silver bg-focus-near-white b--silver dark-gray" : "bg-red white b--red"}`}
                        placeholder="Add new tag..."
                        onKeyDown={(e) => this.validateInput(e)}
                    />
                </li>
            </Anime>
        );
    }
}
